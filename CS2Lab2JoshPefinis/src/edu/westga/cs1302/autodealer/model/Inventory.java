package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

/**
 * The Class Inventory.
 * 
 * @author CS1302
 */
public class Inventory {
	private static final String START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR = "startYear must be <= endYear.";
	private static final String AUTO_CANNOT_BE_NULL = "auto cannot be null.";
	private static final String DEALERSHIP_NAME_CANNOT_BE_EMPTY = "dealershipName cannot be empty.";
	private static final String DEALERSHIP_NAME_CANNOT_BE_NULL = "dealershipName cannot be null.";
	
	private String dealershipName;
	private ArrayList<Automobile> autos;

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public Inventory() {
		this.dealershipName = null;
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition dealershipName cannot be null or empty
	 * @postcondition getDealershipName() == dealershipName AND size() == 0
	 *
	 * @param dealershipName the dealership name
	 */
	public Inventory(String dealershipName) {
		if (dealershipName == null) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_NULL);
		}

		if (dealershipName.isEmpty()) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_EMPTY);
		}

		this.dealershipName = dealershipName;
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Numbers of autos in inventory.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of autos in the inventory.
	 */
	public int size() {
		return this.autos.size();
	}

	/**
	 * Adds the auto to the inventory.
	 * 
	 * @precondition auto != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param auto the auto
	 * @return true, if add successful
	 */
	public boolean add(Automobile auto) {
		if (auto == null) {
			throw new IllegalArgumentException(AUTO_CANNOT_BE_NULL);
		}

		return this.autos.add(auto);
	}

	/**
	 * Find year of oldest auto.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the oldest auto or Integer.MAX_VALUE if no autos
	 */
	public int findYearOfOldestAuto() {
		int oldest = Integer.MAX_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() < oldest) {
				oldest = automobile.getYear();
			}
		}

		return oldest;
	}

	/**
	 * Find year of newest auto.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the newest auto or Integer.MIN_VALUE if no autos
	 */
	public int findYearOfNewestAuto() {
		int newest = Integer.MIN_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() > newest) {
				newest = automobile.getYear();
			}
		}

		return newest;
	}

	/**
	 * Count autos between specified start and end year inclusive.
	 *
	 * @precondition startYear <= endYear
	 * @postcondition none
	 * 
	 * @param startYear the start year
	 * @param endYear   the end year
	 * @return the number of autos between start and end year inclusive.
	 */
	public int countAutosBetween(int startYear, int endYear) {
		if (startYear > endYear) {
			throw new IllegalArgumentException(START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR);
		}
		int count = 0;
		for (Automobile automobile : this.autos) {
			if (automobile.getYear() >= startYear && automobile.getYear() <= endYear) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Gets the dealership name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the dealershipName
	 */
	public String getDealershipName() {
		return this.dealershipName;
	}

	/**
	 * Sets the dealership name.
	 * 
	 * @precondition dealershipName cannot be null or empty
	 * @postcondition getDealershipName() == dealershipName
	 *
	 * @param dealershipName the dealershipName to set
	 */
	public void setDealershipName(String dealershipName) {
		if (dealershipName == null) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_NULL);
		}

		if (dealershipName.isEmpty()) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_EMPTY);
		}

		this.dealershipName = dealershipName;
	}

	/**
	 * Gets the autos.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the autos
	 */
	public ArrayList<Automobile> getAutos() {
		return this.autos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Dealership:" + this.dealershipName + "#autos:" + this.size();
	}
	
	/**
	 * Deleted the specified auto from the inventory
	 * @precondition none
	 * @postcondition if found, size() == size()@prev - 1
	 * @param auto The automobile to delete from the inventory
	 * 
	 * @return true if the auto was found and deleted from the inventory, false
	 * 			otherwise
	 */
	public boolean deleteAuto(Automobile auto) {
		boolean result = false;
		
		if (this.autos.remove(auto)) {
			result = true;
		}
		
		return result;
			
	}
	
	/**
	 * Add all autos in the specified ArrayList to the inventory
	 * 
	 * @precondition list != null
	 * @postcondition this.size == this.size@prev + list.size
	 * 
	 * @param list the ArrayList of automobiles
	 */
	public void addList(ArrayList<Automobile> list) {
		if (list == null) {
			throw new IllegalArgumentException("List cannot be null");
		}
		for (Automobile current : list) {
			this.add(current);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
