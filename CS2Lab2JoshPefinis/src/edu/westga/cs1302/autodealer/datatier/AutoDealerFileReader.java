package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file which is a
 * CSV file with the following format:
 * make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}
		
		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and stores it in
	 * an ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all
	 * autos in the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 *
	 * @throws FileNotFoundException When the specified file cannot be found.
	 */
	public ArrayList<Automobile> loadAllAutos() throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>(); 
		
		try (Scanner scanner = new Scanner(this.inventoryFile)) {
			scanner.useDelimiter(",|\n|\r\n");
			
			while (scanner.hasNext()) {
				String make = scanner.next();
				String model = scanner.next();
				int year = scanner.nextInt();
				double miles = scanner.nextDouble();
				double price = scanner.nextDouble();
				
				Automobile newAuto = new Automobile(make, model, year, miles, price);
				autos.add(newAuto);
			}
		}
		
		return autos;  
	}

}
