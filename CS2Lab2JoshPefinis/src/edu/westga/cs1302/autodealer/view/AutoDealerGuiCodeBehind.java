package edu.westga.cs1302.autodealer.view;

import java.io.File;
import java.io.FileNotFoundException;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.resources.UI;
import edu.westga.cs1302.autodealer.view.converter.PositiveDecimalNumberStringConverter;
import edu.westga.cs1302.autodealer.view.converter.PositiveWholeNumberStringConverter;
import edu.westga.cs1302.autodealer.viewmodel.AutoDealerViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * AutoDealerGuiCodeBehind defines the "controller" for AutoDealerGui.fxml.
 * 
 * @author CS1302
 */
public class AutoDealerGuiCodeBehind {
	private AutoDealerViewModel theViewModel;
	private ObjectProperty<Automobile> selectedAuto;

	@FXML
	private AnchorPane pane;

	@FXML
	private MenuItem fileOpenMenuItem;

	@FXML
	private MenuItem fileSaveMenuItem;

	@FXML
	private TextField makeTextField;

	@FXML
	private TextField modelTextField;

	@FXML
	private TextField yearTextField;

	@FXML
	private TextField milesTextField;

	@FXML
	private TextField priceTextField;

	@FXML
	private Label yearInvalidLabel;

	@FXML
	private Label milesInvalidLabel;

	@FXML
	private Label priceInvalidLabel;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private ListView<Automobile> autosListView;

	@FXML
	private Button deleteButton;

	@FXML
	private TextArea summaryTextArea;

	/**
	 * Instantiates a new auto dealer gui code behind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public AutoDealerGuiCodeBehind() {
		this.theViewModel = new AutoDealerViewModel();
		this.selectedAuto = new SimpleObjectProperty<Automobile>();
	}

	/**
	 * Initializes the GUI components, binding them to the view model properties
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.bindButtonsDisableProperty();
		this.setupListenerForListView();
		this.setupListenersForValidation();

		this.initializeUI();
	}

	private void initializeUI() {
		Integer defaultYear = UI.Text.DEFAULT_YEAR;
		this.yearTextField.setText(defaultYear.toString());
		this.selectedAuto.set(null);
	}

	private void setupListenersForValidation() {
		this.yearTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.isEmpty() && !newValue.matches("[1-2]{0,1}|[1-2]\\d{1,3}")) {
					AutoDealerGuiCodeBehind.this.yearTextField.setText(oldValue);
				}
			}
		});

		this.milesTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue.startsWith(".")) {
					newValue = "0" + newValue;
					AutoDealerGuiCodeBehind.this.milesTextField.setText(newValue);
				}
				if (!newValue.matches("[\\d]*([.]\\d{0,1})?")) {
					AutoDealerGuiCodeBehind.this.milesTextField.setText(oldValue);
				}
			}
		});

		this.priceTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue.startsWith(".")) {
					newValue = "0" + newValue;
					AutoDealerGuiCodeBehind.this.priceTextField.setText(newValue);
				}
				if (!newValue.matches("[\\d]*([.]\\d{0,2})?")) {
					AutoDealerGuiCodeBehind.this.priceTextField.setText(oldValue);
				}
			}
		});
	}

	private void setupListenerForListView() {
		this.autosListView.getSelectionModel().selectedItemProperty().addListener((observable, oldAuto, newAuto) -> {
			if (newAuto != null) {
				this.makeTextField.textProperty().set(newAuto.getMake());
				this.modelTextField.textProperty().set(newAuto.getModel());

				Integer year = newAuto.getYear();
				this.yearTextField.textProperty().set(year.toString());

				Double miles = newAuto.getMiles();
				this.milesTextField.textProperty().set(miles.toString());

				Double price = newAuto.getPrice();

				this.priceTextField.textProperty().set(price.toString());
				this.selectedAuto.set(newAuto);
			}
		});
	}

	private void bindButtonsDisableProperty() {
		BooleanBinding autoSelectedBinding = Bindings
				.isNull(this.autosListView.getSelectionModel().selectedItemProperty());
		this.deleteButton.disableProperty().bind(autoSelectedBinding);
		this.updateButton.disableProperty().bind(autoSelectedBinding);

		this.addButton.disableProperty()
				.bind(this.makeTextField.textProperty().isEmpty()
						.or(this.modelTextField.textProperty().isEmpty()
								.or(this.yearTextField.textProperty().isEmpty().or(this.milesTextField.textProperty()
										.isEmpty().or(this.priceTextField.textProperty().isEmpty())))));
	}

	private void bindComponentsToViewModel() {
		this.makeTextField.textProperty().bindBidirectional(this.theViewModel.makeProperty());
		this.modelTextField.textProperty().bindBidirectional(this.theViewModel.modelProperty());
		this.yearTextField.textProperty().bindBidirectional(this.theViewModel.yearProperty(),
				new PositiveWholeNumberStringConverter());
		this.milesTextField.textProperty().bindBidirectional(this.theViewModel.milesProperty(),
				new PositiveDecimalNumberStringConverter(1));
		this.priceTextField.textProperty().bindBidirectional(this.theViewModel.priceProperty(),
				new PositiveDecimalNumberStringConverter(2));
		this.selectedAuto.bindBidirectional(this.theViewModel.selectedAutoProperty());

		this.autosListView.itemsProperty().bind(this.theViewModel.autosProperty());
		this.summaryTextArea.textProperty().bind(this.theViewModel.inventorySummaryProperty());
	}

	private void showAlert(String title, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		Window owner = this.pane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(message);
		alert.showAndWait();
	}

	@FXML
	private void onAddAuto() {
		try {
			this.theViewModel.addAuto();
		} catch (IllegalArgumentException e) {
			this.showAlert("Add Auto", e.getMessage());
		}
	}

	@FXML
	private void onUpdateAuto() {
		try {
			this.theViewModel.updateAuto();			
		} catch (IllegalArgumentException e) {
			this.showAlert("Update Auto", e.getMessage());
		}
	}

	@FXML
	private void onDeleteAuto() {
		this.theViewModel.deleteAuto();
	}

	@FXML
	private void onFileOpen() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(owner);

		if (selectedFile != null) {
			try {
				this.theViewModel.loadInventory(selectedFile);
			} catch (FileNotFoundException e) {
				this.showAlert("On File Open", e.getMessage());
			}
		}
	}

	@FXML
	private void onFileSave() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(owner);

		if (selectedFile != null) {
			try {
				this.theViewModel.saveInventory(selectedFile);
			} catch (FileNotFoundException e) {
				this.showAlert("on File Save", e.getMessage());
			}
		}
	}

	private void setFileFilters(FileChooser fileChooser) {
		ExtensionFilter filter = new ExtensionFilter("Auto Dealer Inventory", "*.adi");
		fileChooser.getExtensionFilters().add(filter);
		filter = new ExtensionFilter("All Files", "*.*");
		fileChooser.getExtensionFilters().add(filter);
	}

}
