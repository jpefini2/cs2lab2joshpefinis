package edu.westga.cs1302.autodealer.view.output;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";
		String leastExpensive = "";
		String mostExpensive = "";
		
		if (inventory == null) {
			summary = "No inventory to build report for.";
		} else {
			summary = "<" + inventory.getDealershipName() + ">\n#Automobiles: " 
					+ inventory.size();
		}
		
		for (Automobile current : inventory.getAutos()) {
			double lowestPrice = inventory.getAutos().get(0).getPrice();
			
			if (current.getPrice() <= lowestPrice) {
				lowestPrice = current.getPrice();
				leastExpensive = "Least Expensive: " + current.toString();
			}
		}
		
		for (Automobile current : inventory.getAutos()) {
			double highestPrice = inventory.getAutos().get(0).getPrice();
			
			if (current.getPrice() >= highestPrice) {
				highestPrice = current.getPrice();
				mostExpensive = "Most Expensive: " + current.toString();
			}
		}

		return summary + "\n" + leastExpensive + "\n" + mostExpensive;
	}

}


